/**
 * Created by timma on 16.03.2015.
 */


        //Beispiel Modul
        var mymodule = angular.module('myModule', []);

        mymodule.factory('serviceId', function() {
            // ...
            return this;
        });
        mymodule.directive('directiveName',  function() {
            // ...
        });
        mymodule.filter('filterName', function(){
            // ...
        });

        var MyController = function($scope, serviceId) {
            // ...
        };
        MyController.$inject = ['$scope', 'serviceId'];
        mymodule.controller('MyController', MyController);

mymodule.controller('kalenderData', function($scope, $http) {
        $http.get("/angularjs/jsondata.php")
            .success(function (response) {
                    var tr=response;
                    $scope.kalender = tr;
            });
});
//Beispiel Foreach
var values = {name: 'misko', gender: 'male'};
var log = [];
angular.forEach(values, function(value, key) {
    this.push(key + ': ' + value);
}, log);
angular.equals(log,['name: misko', 'gender: male']);

//Beispiel angular Element
var target = angular.element('[id="angular_jquery"]');
console.log(target);