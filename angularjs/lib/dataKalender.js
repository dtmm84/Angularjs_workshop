/**
 * Created by timma on 08.03.2015.
 */
 var  dataBase = [
        {   bereich: "ICC",
            events: [{titel:"Auslug nach köln",
            semester:"WS2014/15",
            language:"DE",
            datum:"22.05.2015",
            zeit:"08h-20h",
            ort:"gießen",
            frei:false,
            link:"",
            beschreibung:"",
            order:3
        },
        {
            titel:"Erasmus Party",
            semester:"WS2014/15",
            language:"DE",
            datum:"28.05.2015",
            zeit:"18h-04h",
            ort:"gießen",
            frei:true,
            link:"",
            beschreibung:"",
            order:2
        },
        {
            titel:"Workshop-how to find a job",
            semester:"WS2014/15",
            language:"DE",
            datum:"12.04.2015",
            zeit:"08h-12h",
            ort:"gießen",
            frei:true,
            link:"",
            beschreibung:"",
            order:1
        },
        {
            titel:"Workshop-how to find a Room",
            semester:"WS2014/15",
            language:"DE",
            datum:"12.07.2015",
            zeit:"08h-12h",
            ort:"gießen",
            frei:true,
            link:"",
            beschreibung:"",
            order:4
        }]
        },
    {
        bereich:"BDP",
        events:[]
    },
    {
        bereich:"OUTP",
        events:[]
    },
    {
        bereich:"INP",
        events:[]
    }


    ];
